## About Developers

Sistema gerado como trabalho de conclusão de matérias na UPF em 2022, planejamento de apresentação para 12/12/2022.

## About Classes 

Projeto Integrador II 2022 - 2º PL
Tópicos Especiais em Desenvolvimento de Sistemas II 2022 - 2º PL

## About Neighbor

Este sistema visa a manutenção de forma descomplicada e descentralizada de ambientes em condomínios. 

## Development

Produzido em Laravel com Banco de Dados MySQL 

## Infra-struture

Rodará em servidor Cloud dedicado, numa VM acessível pela Internet de qualquer dispositivo.

## Access

Acessível atráves de Browsers homologados, como Edge, Chrome e Firefox. 

## Milestones (Macro)

Validação de Requisitos, Classes e Atividades; 
Criar Tabelas e Relacionamentos do Banco;
Modelagem de Telas;
Manutenções de Áreas;
Desenvolvimento de Relatórios;
Autenticação e Segurança de Usuários;
Log de Transações;
Testes de Funcionalidades e Desempenho;
Correção de Defeitos;

