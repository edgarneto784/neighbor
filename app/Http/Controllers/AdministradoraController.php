<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\administradora;
class AdministradoraController extends Controller
{
    public function index()
    {
        $administradoras = administradora::orderBy("nome")->paginate(30);
        return view('administradoras.index', ['administradoras' => $administradoras]);
    }
    public function create(){
        return view('administradoras.create');
    }
    public function store(Request $request){
        $nova_administradora = $request->all();
        administradora::create($nova_administradora);

        return redirect()->route('administradoras');
    }
    public function destroy($id){
        administradora::find($id)->delete();
        return redirect()->route('administradoras');
    }

    public function edit($id){
        $administradora = administradora::find($id);
        return view('administradoras.edit', compact('administradora'));
    }

    public function update(Request $request, $id){
        $administradora = administradora::find($id)->update($request->all());
        return redirect()->route('administradoras');
    }

}
