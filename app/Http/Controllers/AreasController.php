<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Areas;
class AreasController extends Controller
{
    //public function index()
    //{
    //    $areas = Areas::orderBy("nome")->paginate(5);
    //    return view('area.index', ['areas' => $areas]);
    //}
    public function index()
    {
        //$areas = Areas::where('condominio_id', auth()->user()->condominio_id)->orderBy("nome")->paginate(5);
        $areas = Areas::orderBy("nome")->paginate(30);
        return view('area.index', ['areas' => $areas]);
    }
    public function create(){
        return view('area.create');
    }
    public function store(Request $request){
        $nova_area = $request->all();
        Areas::create($nova_area);
        return redirect()->route('areas');
    }
    public function destroy($id){
        Areas::find($id)->delete();
        return redirect()->route('areas');
    }

    public function edit($id){
        $area = Areas::find($id);
        return view('area.edit', compact('area'));
    }

    public function update(Request $request, $id){
        $area = Areas::find($id)->update($request->all());
        return redirect()->route('areas');
    }

}
