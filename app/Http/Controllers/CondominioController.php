<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Condominio;
use App\Models\User;
class CondominioController extends Controller
{
    public function index()
    {
        //$condominios = Condominio::where('imobiliaria_id', auth()->user()->administradora_id)->get();
        $condominios = Condominio::orderBy("nome")->paginate(30);
        return view('condominios.index', ['condominios' => $condominios]);
    }
    //public function index()
    //{
    //    $condominios = Condominio::all();
    //    return view('condominios.index', ['condominios' => $condominios]);
    //}
    public function create(){
        return view('condominios.create');
    }
    public function store(Request $request){
        $novo_condominio = $request->all();
        Condominio::create($novo_condominio);

        return redirect()->route('condominios');
    }
    public function destroy($id){
        Condominio::find($id)->delete();
        return redirect()->route('condominios');
    }

    public function edit($id){
        $condominio = Condominio::find($id);
        return view('condominios.edit', compact('condominio'));
    }

    public function update(Request $request, $id){
        $condominio = Condominio::find($id)->update($request->all());
        return redirect()->route('condominios');
    }
    public function sindico($id){
        $condominio = Condominio::find($id);
        return view('condominios.sindico', compact('condominio'));
    }
    public function update_sindico(Request $request, $id){
        $condominio = Condominio::find($id)->update(['sindico_id' => $request->sindico_id,
                                                    'telefone' => $request->telefone,]);
        $user = User::where('id', $request->sindico_id)->update(['condominio_id' => $id]);
        return redirect()->route('condominios');
    }
}
