<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\administradora;
use App\Models\Condominio;
use App\Models\Areas;
use App\Models\Horarios;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $administradora = administradora::all();
        $condominio = Condominio::all();
        $areas = Areas::all();
        $horario = Horarios::all();
        $user = User::all();
        return view('home.home', ['administradora' => $administradora,'condominio' => $condominio,'areas' => $areas,'horario' => $horario,'user' => $user]);
    }
}
