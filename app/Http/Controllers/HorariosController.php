<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Horarios;
use App\Models\Areas;
use Carbon\Carbon;
class HorariosController extends Controller
{
    public function index(Request $request)
    {
        #$horarios = Horarios::all();
        //$horarios = Horarios::select(Horarios::raw("*,DATE_FORMAT(qtd_tempo, '%Y-%m-%dT%H:%i:%s') as 'inicio',DATE_FORMAT(tempo_final, '%Y-%m-%dT%H:%i:%s') as 'final'"))->where('condominio_id',auth()->user()->condominio_id)->get();
        $horarios = Horarios::select(Horarios::raw("*,DATE_FORMAT(tempo_inicio, '%Y-%m-%dT%H:%i:%s') as 'inicio',DATE_FORMAT(tempo_final, '%Y-%m-%dT%H:%i:%s') as 'final'"))->whereRaw('condominio_id = ? and area_id = ?', [auth()->user()->condominio_id,$request->area_id] )->get();
        $areas = Areas::find($request->area_id);

        $area = $request->area_id;
        return view('horario.index', ['horario' => $horarios,'area' => $area,'areas' => $areas]);
    }
    public function create($id,$tempo_inicio){
        $tempo_inicio1 = Carbon::parse($tempo_inicio)->format('Y-m-d H:i:s');
        $area = Areas::find($id);
        $tempo_final = Carbon::parse($tempo_inicio)->addHours($area->qtd_tempo);
        return view('horario.create',['area' => $area, 'tempo_inicio' => $tempo_inicio1, 'tempo_final' => $tempo_final]);
    }
    public function store(Request $request){
        $date_final = Carbon::parse($request->tempo_inicio)->addHours($request->qtd_tempo);

        $verifica_disponibilidade = Horarios::whereRaw('area_id = ? and tempo_inicio < ? and tempo_final > ?', [$request->area_id,$date_final,$request->tempo_inicio])->get();

        if(count($verifica_disponibilidade) > 0){
            return redirect()->route('areas',['area_id' => $request->area_id])->with('error','Horário indisponível');
        }else{
            
        $horario = new Horarios();
        $horario->tempo_inicio = $request->tempo_inicio;
        $horario->tempo_final = $date_final;
        $horario->condominio_id = auth()->user()->condominio_id;
        $horario->area_id = $request->area_id;
        $horario->usuario_id = auth()->user()->id;
        $horario->save();
            return redirect()->route('areas',['area_id' => $request->area_id])->with('success','Horário cadastrado com sucesso');
        }

    }
    public function destroy($id){
        Horarios::find($id)->delete();
        return redirect()->route('areas');
    }

    public function edit($id){
        $horario = Horarios::find($id);
        return view('horario.edit', compact('horario'));
    }

    public function update(Request $request, $id){

        $inicio = Carbon::parse($request->tempo_inicio);
        $date_final = Carbon::parse($request->tempo_inicio)->addHours($request->qtd_tempo);

        $horario = Horarios::find($id);

        $horario->tempo_inicio = $inicio;
        $horario->tempo_final = $date_final;
        $horario->save();

        //$horarios = Horarios::find($id)->update($request->all());
        return redirect()->route('areas');
    }
}
