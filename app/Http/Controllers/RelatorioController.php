<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Areas;
use App\Models\Condominio;
use App\Models\administradora;
use Illuminate\Support\Facades\Hash;
use Dompdf\Dompdf;
use Barryvdh\DomPDF\Facade\Pdf;
class RelatorioController extends Controller
{
    //public function index()
    //{
    //    $usuarios = User::all();
    //    return view('usuarios.index', ['usuarios' => $usuarios]);
    //}

    public function users()
    {
        $usuarios = User::all();
        $pdf = PDF::loadView('relatorios.users', ['usuarios' => $usuarios]);
        return $pdf->download('relatorio_usuario.pdf');
    }

    public function areas()
    {
        $areas = Areas::all();
        $pdf = PDF::loadView('relatorios.areas', ['areas' => $areas]);
        return $pdf->download('relatorio_areas.pdf');
    }

    public function condominios()
    {
        $condominios = Condominio::all();
        $pdf = PDF::loadView('relatorios.condominios', ['condominios' => $condominios]);
        return $pdf->download('relatorio_condominios.pdf');
    }

    public function administradoras()
    {
        $administradoras = administradora::all();
        $pdf = PDF::loadView('relatorios.administradoras', ['administradoras' => $administradoras]);
        return $pdf->download('relatorio_administradoras.pdf');
    }
        
}
