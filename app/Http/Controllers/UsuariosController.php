<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UsuariosController extends Controller
{
    //public function index()
    //{
    //    $usuarios = User::all();
    //    return view('usuarios.index', ['usuarios' => $usuarios]);
    //}

    public function index()
    {
        $usuarios = User::orderBy("name")->paginate(30);
        return view('usuarios.index', ['usuarios' => $usuarios]);
    }
    public function home()
    {
        return view('home.home');
    }
    public function create(){
        return view('auth.register');
    }

    public function edit($id){
        $usuario = User::find($id);
        return view('usuarios.edit', ['usuario' => $usuario]);
    }

    public function update($id){
        $usuario = User::find($id);
        $usuario->name = request('name');
        $usuario->email = request('email');
        $usuario->save();
        return redirect()->route('usuarios');
    }

    public function store(Request $request){
        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = Hash::make($request->password);
        $usuario->administradora_id = $request->administradora_id;
        $usuario->condominio_id = $request->condominio_id;
        $usuario->permissao = $request->permissao;
        $usuario->save();
        return redirect()->route('usuarios');
    }

    public function destroy($id){
        $usuario = User::find($id);
        $usuario->delete();
        return redirect()->route('usuarios');
    }
}
