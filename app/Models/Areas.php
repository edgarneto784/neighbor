<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    use HasFactory;
    protected $table = "areas";
    protected $fillable = ['nome','qtd_tempo','valor','qtd_pessoas','condominio_id'];

    public function condominio(){
        return $this->belongsTo("App\Models\Condominio", "condominio_id");
    }
}
