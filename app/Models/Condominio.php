<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Condominio extends Model
{
    use HasFactory;

    protected $table = "condominios";
    protected $fillable = ['nome','endereco','imobiliaria_id','sindico_id','telefone'];

    public function imobiliaria(){
        return $this->belongsTo("App\Models\administradora");
    }

    public function areas(){
        return $this->hasMany("App\Models\Areas");
    }
    public function sindico(){
        return $this->belongsTo("App\Models\User");
    }
}
