<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horarios extends Model
{
    use HasFactory;

    protected $table = "horarios";
    protected $fillable = ['qtd_tempo','tempo_final','condominio_id','area_id','usuario_id'];

    public function user(){
        return $this->belongsTo("App\Models\User", "usuario_id");
    }

    public function area(){
        return $this->belongsTo("App\Models\Areas", "area_id");
    }
}
