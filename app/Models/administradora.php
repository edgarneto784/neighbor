<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administradora extends Model
{
    use HasFactory;

    protected $table = "administradoras";
    protected $fillable = ['nome','telefone'];

    public function administradora(){
        return $this->hasMany("App\Models\Condominio");
    }
}
