<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('sudo', function($user){
            return $user->permissao == '1' ? true : false;
        });
        Gate::define('admin', function($user){
            return $user->permissao == '2' ? true : false;
        });
        Gate::define('sindico', function($user){
            return $user->permissao == '3' ? true : false;
        });
        Gate::define('morador', function($user){
            return $user->permissao == '4' ? true : false;
        });
        
    }
}
