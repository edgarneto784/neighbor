-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 30-Nov-2022 às 01:10
-- Versão do servidor: 10.4.25-MariaDB
-- versão do PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `neighbor`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administradoras`
--

CREATE TABLE `administradoras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `administradoras`
--

INSERT INTO `administradoras` (`id`, `nome`, `telefone`, `created_at`, `updated_at`) VALUES
(1, 'Imobiliária Teste 22 11 22', 54221122, '2022-11-23 02:09:03', '2022-11-23 02:09:03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `areas`
--

CREATE TABLE `areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `qtd_pessoas` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condominio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `qtd_tempo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `areas`
--

INSERT INTO `areas` (`id`, `nome`, `valor`, `qtd_pessoas`, `created_at`, `updated_at`, `condominio_id`, `qtd_tempo`) VALUES
(1, 'Futebol Society', 0, 10, '2022-11-23 02:15:12', '2022-11-23 02:15:12', 1, 1),
(2, 'Piscina', 0, 1, '2022-11-23 02:15:27', '2022-11-23 02:15:27', 1, 1),
(3, 'Piscinão de Véio', 0, 16, '2022-11-23 03:13:16', '2022-11-23 03:13:16', 2, 1),
(4, 'Piscina Sindico Novo 2', 15, 10, '2022-11-28 04:15:36', '2022-11-28 04:15:36', 1, 2),
(5, 'Piscina Sindico Novo 2', 0, 10, '2022-11-28 04:15:50', '2022-11-28 04:15:50', 2, 2),
(6, 'teste', 15, 10, '2022-11-28 05:20:17', '2022-11-28 05:20:17', 2, 1),
(7, 'Aula 28 11 22', 100, 30, '2022-11-29 02:35:54', '2022-11-29 02:35:54', 1, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `condominios`
--

CREATE TABLE `condominios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Endereco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `imobiliaria_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sindico_id` bigint(20) UNSIGNED DEFAULT NULL,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `condominios`
--

INSERT INTO `condominios` (`id`, `nome`, `Endereco`, `created_at`, `updated_at`, `imobiliaria_id`, `sindico_id`, `telefone`) VALUES
(1, 'Condomínio 22 11 22', 'Rua 22 11 22', '2022-11-23 02:11:31', '2022-11-23 02:13:31', 1, 3, NULL),
(2, 'Condomínio 22 11 22 - 2', 'Rua 22 11 22 - 2', '2022-11-23 03:01:25', '2022-11-27 20:36:17', 1, 7, NULL),
(3, 'Condomínio 22 11 22 - 3', 'Rua 22 11 22 - 3', '2022-11-23 03:04:22', '2022-11-23 03:11:44', 1, 8, NULL),
(4, 'Condomínio 22 11 22 - 4', 'Rua 22 11 22 - 4', '2022-11-23 03:12:12', '2022-11-27 22:46:33', 1, 9, '(54) 99222-8582');

-- --------------------------------------------------------

--
-- Estrutura da tabela `condominiousers`
--

CREATE TABLE `condominiousers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `condominio_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `horarios`
--

CREATE TABLE `horarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condominio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `usuario_id` bigint(20) UNSIGNED DEFAULT NULL,
  `area_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tempo_inicio` datetime DEFAULT NULL,
  `tempo_final` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `horarios`
--

INSERT INTO `horarios` (`id`, `created_at`, `updated_at`, `condominio_id`, `usuario_id`, `area_id`, `tempo_inicio`, `tempo_final`) VALUES
(1, '2022-11-23 02:17:38', '2022-11-23 02:17:38', 1, 3, 1, '2022-11-25 20:00:00', '2022-11-25 21:00:00'),
(12, '2022-11-27 19:35:51', '2022-11-27 19:35:51', 2, 6, 3, '2022-11-27 03:00:00', '2022-11-27 04:00:00'),
(13, '2022-11-27 19:37:07', '2022-11-27 19:37:07', 2, 6, 3, '2022-11-27 05:00:00', '2022-11-27 06:00:00'),
(23, '2022-11-28 03:18:11', '2022-11-28 03:18:11', 2, 6, 3, '2022-11-28 15:30:00', '2022-11-28 16:30:00'),
(33, '2022-11-28 04:23:43', '2022-11-28 04:23:43', 2, 4, 3, '2022-11-26 03:00:00', '2022-11-26 04:00:00'),
(34, '2022-11-28 04:23:56', '2022-11-28 04:23:56', 2, 4, 3, '2022-11-28 03:00:00', '2022-11-28 04:00:00'),
(35, '2022-11-28 04:24:12', '2022-11-29 02:08:57', 2, 4, 3, '2022-12-01 16:00:00', '2022-12-01 17:00:00'),
(36, '2022-11-29 02:06:48', '2022-11-29 02:06:48', 2, 4, 6, '2022-11-28 10:00:00', '2022-11-28 11:00:00'),
(37, '2022-11-29 02:36:37', '2022-11-29 02:36:37', 1, 3, 7, '2022-12-02 19:00:00', '2022-12-02 22:00:00'),
(38, '2022-11-30 02:05:27', '2022-11-30 02:05:27', 1, 12, 2, '2022-12-02 18:00:00', '2022-12-02 19:00:00'),
(39, '2022-11-30 02:05:41', '2022-11-30 02:05:41', 1, 12, 7, '2022-11-27 07:00:00', '2022-11-27 10:00:00'),
(40, '2022-11-30 02:06:41', '2022-11-30 02:06:41', 1, 12, 1, '2022-12-03 13:00:00', '2022-12-03 14:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_12_230612_create_administradoras_table', 1),
(6, '2022_09_12_230626_create_condominios_table', 1),
(7, '2022_09_12_230639_create_areas_table', 1),
(8, '2022_09_12_230651_create_horarios_table', 1),
(9, '2022_09_12_230706_create_usuarios_table', 1),
(10, '2022_09_13_001348_create_condominiousers_table', 1),
(11, '2022_10_09_165636_remove_imobiliaria_id_to_condominios', 1),
(12, '2022_10_17_235447_add_imobiliario_to_condominios', 1),
(13, '2022_10_23_171315_remove_condominio_id_to_areas', 1),
(14, '2022_10_23_171444_add_condominio_id_to_areas', 1),
(15, '2022_10_25_232203_remove_usuario_id_to_administradoras', 1),
(16, '2022_10_29_175157_add_fields_to_users', 1),
(17, '2022_10_29_180807_add_permissao_to_users', 1),
(18, '2022_10_29_200844_add_sindico_to_condominios', 1),
(19, '2022_11_05_171634_remove_fields_to_horarios', 1),
(20, '2022_11_05_171643_add_fields_to_horarios', 1),
(21, '2022_11_05_173050_remove_qtd_tempo_to_areas', 1),
(22, '2022_11_05_173110_add_qtd_tempo_to_areas', 1),
(23, '2022_11_12_175653_remove_qtd_tempo3_to_areas', 1),
(24, '2022_11_12_175700_add_qtd_tempo3_to_areas', 1),
(25, '2022_11_12_180510_remove_qtd_tempo_to_horarios', 1),
(26, '2022_11_12_180539_add_tempo_inicio_to_horarios', 1),
(27, '2022_11_14_162300_add_tempo_final_to_horarios', 1),
(28, '2022_11_27_172255_add_telefone_to_condominios', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('186272@upf.br', '$2y$10$b16smN82zbVmUDgMc6ptXebGarZO0f.AszeBV6Cx.DEbKR8E.2xP6', '2022-11-28 03:27:42'),
('emerson-rocha.2@hotmail.com.br', '$2y$10$UkEKqeOsUbz1ObRVX4Lp1OXWB7a86bcqFICdkwXnmB1z4fcg2dgY2', '2022-11-29 02:50:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condominio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `administradora_id` bigint(20) UNSIGNED DEFAULT NULL,
  `permissao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `condominio_id`, `administradora_id`, `permissao`) VALUES
(1, 'Admin', 'admin@neighbor.com', NULL, '$2y$10$f0.e/6D6BOEEoYr9kCKTu.1Yjpapk5hxmy2Iv3b1zuxYA85YA2FKu', NULL, '2022-11-14 19:13:33', '2022-11-14 19:13:33', NULL, NULL, 1),
(2, 'Corretor 22 11 22', 'corretor221122@neighbor.com', NULL, '$2y$10$47ovWz25uNKUiK/y9BfET.v1n4y2fRzExOERdea5pFxLkHU/bSllm', NULL, '2022-11-23 02:10:09', '2022-11-23 02:10:09', NULL, 1, 2),
(3, 'Sindico 22 11 22', 'sindico221122@neighbor.com', NULL, '$2y$10$sPwj/J6zR9DiOMEboeQQ/e3NdZra8jXcGaxk/LAXYzT.AAHMnFvDS', NULL, '2022-11-23 02:12:53', '2022-11-23 02:13:31', 1, 1, 3),
(4, 'Condômino 22 11 22', 'condomino221122@neighbor.com', NULL, '$2y$10$BxiKRO8H7Xy8..MDSerYCu9Y3N.8yZIx0GvJKtB6dScpeXmwg06X.', NULL, '2022-11-23 02:16:52', '2022-11-23 02:16:52', 2, 1, 4),
(5, 'corretor22 11 22 2', 'corretor221122-2@neighbor.com', NULL, '$2y$10$dLwo21R6cenewsdkFYyRG.SzOs.vHvF.avqa0UNxJV1Ofg2kDkFrq', NULL, '2022-11-23 03:00:10', '2022-11-23 03:00:10', NULL, 1, 2),
(6, 'sindico 22 11 22 - 2', 'sindico221122-2@neighbor.com', NULL, '$2y$10$jaIZ6GLS6lKcuAU/9AMHsuIjDbgVtdFqO/cY1cbFeHWMWhm9UAs0O', NULL, '2022-11-23 03:02:46', '2022-11-23 03:03:14', 2, 1, 3),
(7, 'sindico221122-3', 'sindico221122-3@neighbor.com', NULL, '$2y$10$XP/BKR3efXNd433bBtxQ3uk.U3SbZ7Y2T.GcVAhsme5kEoT64zTIC', NULL, '2022-11-23 03:05:45', '2022-11-27 20:36:17', 2, 1, 3),
(8, 'sindico221122-4', 'sindico221122-4@neighbor.com', NULL, '$2y$10$1CCX99QhW5iZESnSnbPazOAr/v1lOm/XaU8jzbNSLYd6tuw/rmhk.', NULL, '2022-11-23 03:11:28', '2022-11-23 03:11:44', 3, 1, 3),
(9, 'sindico novo', 'sindico27112022@neighbor.com', NULL, '$2y$10$1WAEcjWTwYf7zqEjYI2OZe58MZiaMeR2abQLD9huCSZlvs0yN6Ctq', NULL, '2022-11-27 20:59:44', '2022-11-27 22:46:33', 4, 1, 3),
(10, 'sindico novo2', 'emerson-rocha.2@hotmail.com.br', NULL, '$2y$10$.Pv2MlO3fwpRgrKcCH3PiufBuqJnPtC4BTCp2/DTQ/Ccqc9Zyn5vS', NULL, '2022-11-27 22:47:57', '2022-11-27 22:47:57', NULL, 1, 3),
(11, 'Edgar UPF', '186272@upf.br', NULL, '$2y$10$90T1jq81Tf5EzFgFC75XSOT5rt.MN4FL2nKkJCq7lsRiBLXYUjbRy', NULL, '2022-11-28 03:25:44', '2022-11-28 03:25:44', NULL, 1, 2),
(12, 'Morador 29 11 22', 'morador291122@neighbor.com', NULL, '$2y$10$8UkTV2G66uMM5UZ2CfkPO.gieIlQ71/LtgSgb4bnTcDzfVn9MjMN2', NULL, '2022-11-30 02:04:23', '2022-11-30 02:04:23', 1, 1, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissao` int(11) NOT NULL,
  `condominio_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `administradoras`
--
ALTER TABLE `administradoras`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `areas_condominio_id_foreign` (`condominio_id`);

--
-- Índices para tabela `condominios`
--
ALTER TABLE `condominios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `condominios_imobiliaria_id_foreign` (`imobiliaria_id`),
  ADD KEY `condominios_sindico_id_foreign` (`sindico_id`);

--
-- Índices para tabela `condominiousers`
--
ALTER TABLE `condominiousers`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Índices para tabela `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `horarios_usuario_id_foreign` (`usuario_id`),
  ADD KEY `horarios_area_id_foreign` (`area_id`),
  ADD KEY `horarios_condominio_id_foreign` (`condominio_id`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices para tabela `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_condominio_id_foreign` (`condominio_id`),
  ADD KEY `users_administradora_id_foreign` (`administradora_id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `administradoras`
--
ALTER TABLE `administradoras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `areas`
--
ALTER TABLE `areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `condominios`
--
ALTER TABLE `condominios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `condominiousers`
--
ALTER TABLE `condominiousers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de tabela `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_condominio_id_foreign` FOREIGN KEY (`condominio_id`) REFERENCES `condominios` (`id`);

--
-- Limitadores para a tabela `condominios`
--
ALTER TABLE `condominios`
  ADD CONSTRAINT `condominios_imobiliaria_id_foreign` FOREIGN KEY (`imobiliaria_id`) REFERENCES `administradoras` (`id`),
  ADD CONSTRAINT `condominios_sindico_id_foreign` FOREIGN KEY (`sindico_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `horarios`
--
ALTER TABLE `horarios`
  ADD CONSTRAINT `horarios_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `horarios_condominio_id_foreign` FOREIGN KEY (`condominio_id`) REFERENCES `condominios` (`id`),
  ADD CONSTRAINT `horarios_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_administradora_id_foreign` FOREIGN KEY (`administradora_id`) REFERENCES `administradoras` (`id`),
  ADD CONSTRAINT `users_condominio_id_foreign` FOREIGN KEY (`condominio_id`) REFERENCES `condominios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
