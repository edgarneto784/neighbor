-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 30-Nov-2022 às 03:08
-- Versão do servidor: 10.4.25-MariaDB
-- versão do PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `neighbor`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administradoras`
--

CREATE TABLE `administradoras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `administradoras`
--

INSERT INTO `administradoras` (`id`, `nome`, `telefone`, `created_at`, `updated_at`) VALUES
(2, 'Imobiliária ABC', 548877552, '2022-11-30 04:11:59', '2022-11-30 04:11:59'),
(3, 'Imobiliária DE', 548877553, '2022-11-30 04:14:30', '2022-11-30 04:14:30'),
(4, 'Imobiliária F', 548877557, '2022-11-30 04:14:56', '2022-11-30 04:14:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `areas`
--

CREATE TABLE `areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `qtd_pessoas` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condominio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `qtd_tempo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `areas`
--

INSERT INTO `areas` (`id`, `nome`, `valor`, `qtd_pessoas`, `created_at`, `updated_at`, `condominio_id`, `qtd_tempo`) VALUES
(8, 'Area A1', 0, 10, '2022-11-30 04:26:31', '2022-11-30 04:26:31', NULL, 1),
(9, 'Area A1', 1, 10, '2022-11-30 04:26:48', '2022-11-30 04:26:48', NULL, 1),
(10, 'Area A1', 0, 10, '2022-11-30 04:27:35', '2022-11-30 04:27:35', NULL, 1),
(11, 'Area A1', 0, 10, '2022-11-30 04:30:10', '2022-11-30 04:30:10', 6, 1),
(12, 'Area A2', 45, 6, '2022-11-30 04:30:24', '2022-11-30 04:30:24', 6, 2),
(13, 'Area A3', 65, 8, '2022-11-30 04:30:39', '2022-11-30 04:30:39', 6, 4),
(14, 'Area B1', 0, 2, '2022-11-30 04:36:52', '2022-11-30 04:36:52', 7, 2),
(15, 'Area C1', 250, 12, '2022-11-30 04:45:31', '2022-11-30 04:45:31', 8, 6),
(16, 'Area C2', 0, 1, '2022-11-30 04:45:43', '2022-11-30 04:45:43', 8, 1),
(17, 'Area D1', 100, 12, '2022-11-30 04:55:50', '2022-11-30 04:55:50', 9, 6),
(18, 'Area D2', 120, 18, '2022-11-30 04:56:06', '2022-11-30 04:56:06', 9, 1),
(19, 'Area E1', 28, 6, '2022-11-30 05:00:59', '2022-11-30 05:00:59', 10, 1),
(20, 'Area F1', 25, 12, '2022-11-30 05:05:51', '2022-11-30 05:05:51', 11, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `condominios`
--

CREATE TABLE `condominios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Endereco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `imobiliaria_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sindico_id` bigint(20) UNSIGNED DEFAULT NULL,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `condominios`
--

INSERT INTO `condominios` (`id`, `nome`, `Endereco`, `created_at`, `updated_at`, `imobiliaria_id`, `sindico_id`, `telefone`) VALUES
(6, 'Condomínio A', 'Rua A, Número A.', '2022-11-30 04:22:34', '2022-11-30 04:29:10', 2, 22, '(54) 77885-5222'),
(7, 'Condominio B', 'Rua B, Número B.', '2022-11-30 04:22:48', '2022-11-30 04:29:23', 2, 23, '(87) 45454-5454'),
(8, 'Condominio C', 'Rua C, Número C.', '2022-11-30 04:23:00', '2022-11-30 04:29:34', 2, 24, '(96) 52359-6794'),
(9, 'Condominio D', 'Rua D, Número D.', '2022-11-30 04:52:44', '2022-11-30 04:55:03', 3, 37, '(32) 65897-8795'),
(10, 'Condominio E', 'Rua E, Número E.', '2022-11-30 04:52:58', '2022-11-30 04:55:14', 3, 38, '(54) 89798-4654'),
(11, 'Condominio F', 'Rua F, Número F.', '2022-11-30 05:04:26', '2022-11-30 05:05:07', 4, 44, '(65) 89856-5989');

-- --------------------------------------------------------

--
-- Estrutura da tabela `condominiousers`
--

CREATE TABLE `condominiousers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `condominio_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `horarios`
--

CREATE TABLE `horarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condominio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `usuario_id` bigint(20) UNSIGNED DEFAULT NULL,
  `area_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tempo_inicio` datetime DEFAULT NULL,
  `tempo_final` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `horarios`
--

INSERT INTO `horarios` (`id`, `created_at`, `updated_at`, `condominio_id`, `usuario_id`, `area_id`, `tempo_inicio`, `tempo_final`) VALUES
(41, '2022-11-30 04:31:02', '2022-11-30 04:31:02', 6, 22, 11, '2022-12-03 10:00:00', '2022-12-03 11:00:00'),
(42, '2022-11-30 04:33:50', '2022-11-30 04:33:50', 6, 25, 12, '2022-12-04 18:00:00', '2022-12-04 20:00:00'),
(43, '2022-11-30 04:34:33', '2022-11-30 04:34:33', 6, 26, 13, '2022-12-07 16:00:00', '2022-12-07 20:00:00'),
(44, '2022-11-30 04:35:26', '2022-11-30 04:35:26', 6, 27, 12, '2022-12-07 16:00:00', '2022-12-07 18:00:00'),
(45, '2022-11-30 04:36:00', '2022-11-30 04:36:00', 6, 28, 13, '2022-12-10 21:00:00', '2022-12-11 01:00:00'),
(46, '2022-11-30 04:37:05', '2022-11-30 04:37:05', 7, 23, 14, '2022-12-17 23:00:00', '2022-12-18 01:00:00'),
(47, '2022-11-30 04:41:44', '2022-11-30 04:41:44', 7, 29, 14, '2022-12-14 16:00:00', '2022-12-14 18:00:00'),
(48, '2022-11-30 04:42:37', '2022-11-30 04:42:37', 7, 31, 14, '2022-12-05 10:00:00', '2022-12-05 12:00:00'),
(49, '2022-11-30 04:45:56', '2022-11-30 04:45:56', 8, 24, 15, '2022-12-12 10:00:00', '2022-12-12 16:00:00'),
(50, '2022-11-30 04:48:18', '2022-11-30 04:48:37', 8, 32, 16, '2022-12-13 16:00:00', '2022-12-13 17:00:00'),
(51, '2022-11-30 04:49:32', '2022-11-30 04:49:32', 8, 33, 15, '2022-12-09 12:00:00', '2022-12-09 18:00:00'),
(52, '2022-11-30 04:50:14', '2022-11-30 04:50:14', 8, 34, 16, '2022-12-15 18:00:00', '2022-12-15 19:00:00'),
(53, '2022-11-30 04:50:48', '2022-11-30 04:50:48', 8, 35, 16, '2022-12-07 13:00:00', '2022-12-07 14:00:00'),
(54, '2022-11-30 04:51:45', '2022-11-30 04:51:45', 8, 36, 15, '2022-12-06 17:00:00', '2022-12-06 23:00:00'),
(55, '2022-11-30 04:56:19', '2022-11-30 04:56:19', 9, 37, 17, '2022-12-15 18:00:00', '2022-12-16 00:00:00'),
(56, '2022-11-30 04:58:53', '2022-11-30 04:58:53', 9, 39, 17, '2022-12-25 22:00:00', '2022-12-26 04:00:00'),
(57, '2022-11-30 04:59:26', '2022-11-30 04:59:26', 9, 40, 17, '2022-12-18 15:00:00', '2022-12-18 21:00:00'),
(58, '2022-11-30 05:00:11', '2022-11-30 05:00:11', 9, 41, 18, '2022-12-17 20:00:00', '2022-12-17 21:00:00'),
(59, '2022-11-30 05:02:34', '2022-11-30 05:02:34', 10, 38, 19, '2022-12-20 20:00:00', '2022-12-20 21:00:00'),
(60, '2022-11-30 05:03:13', '2022-11-30 05:03:13', 10, 42, 19, '2022-12-18 23:00:00', '2022-12-19 00:00:00'),
(61, '2022-11-30 05:03:46', '2022-11-30 05:03:46', 10, 43, 19, '2022-12-22 19:00:00', '2022-12-22 20:00:00'),
(62, '2022-11-30 05:06:08', '2022-11-30 05:06:08', 11, 44, 20, '2022-12-15 18:00:00', '2022-12-15 22:00:00'),
(63, '2022-11-30 05:07:20', '2022-11-30 05:07:20', 11, 45, 20, '2022-12-16 20:00:00', '2022-12-17 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_12_230612_create_administradoras_table', 1),
(6, '2022_09_12_230626_create_condominios_table', 1),
(7, '2022_09_12_230639_create_areas_table', 1),
(8, '2022_09_12_230651_create_horarios_table', 1),
(9, '2022_09_12_230706_create_usuarios_table', 1),
(10, '2022_09_13_001348_create_condominiousers_table', 1),
(11, '2022_10_09_165636_remove_imobiliaria_id_to_condominios', 1),
(12, '2022_10_17_235447_add_imobiliario_to_condominios', 1),
(13, '2022_10_23_171315_remove_condominio_id_to_areas', 1),
(14, '2022_10_23_171444_add_condominio_id_to_areas', 1),
(15, '2022_10_25_232203_remove_usuario_id_to_administradoras', 1),
(16, '2022_10_29_175157_add_fields_to_users', 1),
(17, '2022_10_29_180807_add_permissao_to_users', 1),
(18, '2022_10_29_200844_add_sindico_to_condominios', 1),
(19, '2022_11_05_171634_remove_fields_to_horarios', 1),
(20, '2022_11_05_171643_add_fields_to_horarios', 1),
(21, '2022_11_05_173050_remove_qtd_tempo_to_areas', 1),
(22, '2022_11_05_173110_add_qtd_tempo_to_areas', 1),
(23, '2022_11_12_175653_remove_qtd_tempo3_to_areas', 1),
(24, '2022_11_12_175700_add_qtd_tempo3_to_areas', 1),
(25, '2022_11_12_180510_remove_qtd_tempo_to_horarios', 1),
(26, '2022_11_12_180539_add_tempo_inicio_to_horarios', 1),
(27, '2022_11_14_162300_add_tempo_final_to_horarios', 1),
(28, '2022_11_27_172255_add_telefone_to_condominios', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('186272@upf.br', '$2y$10$b16smN82zbVmUDgMc6ptXebGarZO0f.AszeBV6Cx.DEbKR8E.2xP6', '2022-11-28 03:27:42'),
('emerson-rocha.2@hotmail.com.br', '$2y$10$UkEKqeOsUbz1ObRVX4Lp1OXWB7a86bcqFICdkwXnmB1z4fcg2dgY2', '2022-11-29 02:50:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `condominio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `administradora_id` bigint(20) UNSIGNED DEFAULT NULL,
  `permissao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `condominio_id`, `administradora_id`, `permissao`) VALUES
(1, 'Admin', 'admin@neighbor.com', NULL, '$2y$10$f0.e/6D6BOEEoYr9kCKTu.1Yjpapk5hxmy2Iv3b1zuxYA85YA2FKu', '3zcL0rq4D9tjRZRiwkHVQO7GX8q89xbliTf3IHoKu9DG9SBcIQTvF4xPqlT2', '2022-11-14 19:13:33', '2022-11-14 19:13:33', NULL, NULL, 1),
(16, 'Corretor ABC 1', 'corretorabc1@neighbor.com', NULL, '$2y$10$FR70nlLsDsJSJ3y/b8P/N.TH0W.rzlBhZbqwQk7FgCgscJtw0Kjtu', '3pu8g3y7ZdBvm0ivEuak6lH72YbxXV0zM80joWxK02XbBb3vmFyeM9dHjKy3', '2022-11-30 04:12:40', '2022-11-30 04:12:40', NULL, 2, 2),
(17, 'Corretor ABC 2', 'corretorabc2@neighbor.com', NULL, '$2y$10$wVnpW5obKoyVJQ6Yryp2OOjboauImIE4tZyOznFDYRqpJLzW9exY.', NULL, '2022-11-30 04:13:35', '2022-11-30 04:13:35', NULL, 2, 2),
(18, 'Corretor DE 1', 'corretorde1@neighbor.com', NULL, '$2y$10$T3MaKugVuEtJ/RKda6p4nu8GXK69jda75Zv1IOq/dLRuQeA6oZWum', NULL, '2022-11-30 04:15:45', '2022-11-30 04:15:45', NULL, 3, 2),
(19, 'Corretor DE 2', 'corretorde2@neighbor.com', NULL, '$2y$10$TUcs9En8F4eSwf5l2J.E7u5OXblfnOt4X/HjGwsvpPHOSLCPKsTGu', NULL, '2022-11-30 04:15:58', '2022-11-30 04:15:58', NULL, 3, 2),
(20, 'Corretor DE 3', 'corretorde3@neighbor.com', NULL, '$2y$10$249GfU5JUUVzDsUTs6J54.HCyYlryzGLl44/zApCWh9cav/BDVSWq', NULL, '2022-11-30 04:16:20', '2022-11-30 04:16:20', NULL, 3, 2),
(21, 'Corretor F 1', 'corretorf1@neighbor.com', NULL, '$2y$10$HCnser/nSHt2kez4J3rYvuay6sGdPwTj5efYMZrsxXw7HxyseDol2', NULL, '2022-11-30 04:16:57', '2022-11-30 04:16:57', NULL, 4, 2),
(22, 'Sindico A', 'sindicoa@neighbor.com', NULL, '$2y$10$WJQVgxUZLSd2kJpdGceGa.jt/6VHYA1ksRclQ.MCH4F4WPrXLL1r.', NULL, '2022-11-30 04:24:00', '2022-11-30 04:29:10', 6, 2, 3),
(23, 'Sindico B', 'sindicob@neighbor.com', NULL, '$2y$10$/zofoz3Dof8MnTChuRx32e5zW5DNM7Jvy7Q9k8Fc4pdWdRbEP86O.', NULL, '2022-11-30 04:25:11', '2022-11-30 04:29:23', 7, 2, 3),
(24, 'Sindico C', 'sindicoc@neighbor.com', NULL, '$2y$10$iJXoLPXdUffXpDL7p8KnJuhT6mHaksob1PtFbVWWFtUlXlgGrAOcG', NULL, '2022-11-30 04:25:34', '2022-11-30 04:29:34', 8, 2, 3),
(25, 'Morador A1', 'moradora1@neighbor.com', NULL, '$2y$10$gB8CRyRi0R9FoHChMLkLx.fCPi4IU3eGKXsXRDZcnBIIZS225IwMG', NULL, '2022-11-30 04:31:25', '2022-11-30 04:31:25', 6, 2, 4),
(26, 'Morador A2', 'moradora2@neighbor.com', NULL, '$2y$10$PworBGAmD4FYiTKrZic0NucThgPMZSVYhU8Z.6gQXjftdQbu23SXW', NULL, '2022-11-30 04:31:59', '2022-11-30 04:31:59', 6, 2, 4),
(27, 'Morador A3', 'moradora3@neighbor.com', NULL, '$2y$10$P/KFdiQwXP7FXNTBVQEp1.x.LWdfmcqi4s8cpqYw4rSNsc3dKjUZa', NULL, '2022-11-30 04:32:37', '2022-11-30 04:32:37', 6, 2, 4),
(28, 'Morador A4', 'moradora4@neighbor.com', NULL, '$2y$10$oBUOD1i2xYUDtLNVzXUu7eU59S/hH.HwYQC7rTRzK7.yQ7jo7g4CW', NULL, '2022-11-30 04:32:50', '2022-11-30 04:32:50', 6, 2, 4),
(29, 'Morador B1', 'moradorb1@neighbor.com', NULL, '$2y$10$wOCN3fN3Ph51KB3oAEBn3.d/Efi4twiJXhG6CUnT4QaETqERY88Ie', NULL, '2022-11-30 04:37:27', '2022-11-30 04:37:27', 7, 2, 4),
(31, 'Morador B2', 'moradorb2@neighbor.com', NULL, '$2y$10$PQMwEoaOXl6ZQwAhy7FblOUUB7UEQZ322PnHJ4utwqVs8MqrG2XMa', NULL, '2022-11-30 04:39:33', '2022-11-30 04:39:33', 7, 2, 4),
(32, 'Morador C1', 'moradorc1@neighbor.com', NULL, '$2y$10$NuAUnILLvLNt9/G40L5sKeTqUc7Tb9H8OM47p9b0PdH481DrbhBXm', NULL, '2022-11-30 04:46:23', '2022-11-30 04:46:23', 8, 2, 4),
(33, 'Morador C2', 'moradorc2@neighbor.com', NULL, '$2y$10$79sjkRkowQBXyaCE8Zw8LeWCRoO.uHW9/b4rORBUi7aueZ1Iqi3c.', NULL, '2022-11-30 04:46:47', '2022-11-30 04:46:47', 8, 2, 4),
(34, 'Morador C3', 'moradorc3@neighbor.com', NULL, '$2y$10$d7L6qjlOOoMjA3oG5VJIk.LFRmWfb2vgOcnXWkhWwYsoRhxFEXKsO', NULL, '2022-11-30 04:47:07', '2022-11-30 04:47:07', 8, 2, 4),
(35, 'Morador C4', 'moradorc4@neighbor.com', NULL, '$2y$10$EHNmGJ3hFVqK.X.7RY9FQeJoL01/kP8SAt.FYaVNqc24JDsobu1Yi', NULL, '2022-11-30 04:47:21', '2022-11-30 04:47:21', 8, 2, 4),
(36, 'Morador C5', 'moradorc5@neighbor.com', NULL, '$2y$10$Sr6Ocudn0UJQABzhNXLtMuDDHdG0gwcNwmyX2yQCroEebyJOSy6LS', NULL, '2022-11-30 04:47:35', '2022-11-30 04:47:35', 8, 2, 4),
(37, 'Sindico D', 'sindicod@neighbor.com', NULL, '$2y$10$gxi0U7tM1FFrv5wx6AHY9OL8kXHDlTFPTycslbZBSYpaR2m9xrWNS', NULL, '2022-11-30 04:53:24', '2022-11-30 04:55:03', 9, 3, 3),
(38, 'Sindico E', 'sindicoe@neighbor.com', NULL, '$2y$10$1xn8oplzpytcMfv4jSND7e12bTfXigPuRouP3kzScPnhg2JCiQi5C', NULL, '2022-11-30 04:53:55', '2022-11-30 04:55:14', 10, 3, 3),
(39, 'Morador D1', 'moradord1@neighbor.com', NULL, '$2y$10$DSJTP6YU3Qf8ug5QxNNIwuFmn44fqRnWq/73HsSQXCKMQ52vTvvOi', NULL, '2022-11-30 04:57:06', '2022-11-30 04:57:06', 9, 3, 4),
(40, 'Morador D2', 'moradord2@neighbor.com', NULL, '$2y$10$pACqxHeIpzXbgnIcWFFAJOUuLUaBgTsKPE3sS8VroUqL7jzL4Or26', NULL, '2022-11-30 04:57:27', '2022-11-30 04:57:27', 9, 3, 4),
(41, 'Morador D3', 'moradord3@neighbor.com', NULL, '$2y$10$nCISC7yrccVwnDIhWbKjDuKGXqwmA7jhVijDPiKbhMwFroPknlSoC', NULL, '2022-11-30 04:57:54', '2022-11-30 04:57:54', 9, 3, 4),
(42, 'Morador E1', 'moradore1@neighbor.com', NULL, '$2y$10$lKt5cf/DxC8CuM4Owucree.YBxldSyPhoHrFjx1Uwafo2jeTM1C/G', NULL, '2022-11-30 05:01:25', '2022-11-30 05:01:25', 10, 3, 4),
(43, 'Morador E2', 'moradore2@neighbor.com', NULL, '$2y$10$D27N0VEVqV3s5qPwNcBXi.mkFpcFh27WcKMjAmfrBX8BrNdmSaO4G', NULL, '2022-11-30 05:01:45', '2022-11-30 05:01:45', 10, 3, 4),
(44, 'Sindico F', 'sindicoF@neighbor.com', NULL, '$2y$10$2AmXgADBZ1c4w2PDccmwy.QlJg8F1SBSYDxB45SkfePHXbiZMKCSe', NULL, '2022-11-30 05:04:47', '2022-11-30 05:05:07', 11, 4, 3),
(45, 'Morador F1', 'moradorf1@neighbor.com', NULL, '$2y$10$Dl2sk2bU8bUQxhnvS75rm.uaAoYydR7vIoOhUvyxnL6P71oezeyEa', NULL, '2022-11-30 05:06:23', '2022-11-30 05:06:23', 11, 4, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissao` int(11) NOT NULL,
  `condominio_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `administradoras`
--
ALTER TABLE `administradoras`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `areas_condominio_id_foreign` (`condominio_id`);

--
-- Índices para tabela `condominios`
--
ALTER TABLE `condominios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `condominios_imobiliaria_id_foreign` (`imobiliaria_id`),
  ADD KEY `condominios_sindico_id_foreign` (`sindico_id`);

--
-- Índices para tabela `condominiousers`
--
ALTER TABLE `condominiousers`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Índices para tabela `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `horarios_usuario_id_foreign` (`usuario_id`),
  ADD KEY `horarios_area_id_foreign` (`area_id`),
  ADD KEY `horarios_condominio_id_foreign` (`condominio_id`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices para tabela `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_condominio_id_foreign` (`condominio_id`),
  ADD KEY `users_administradora_id_foreign` (`administradora_id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `administradoras`
--
ALTER TABLE `administradoras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `areas`
--
ALTER TABLE `areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de tabela `condominios`
--
ALTER TABLE `condominios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `condominiousers`
--
ALTER TABLE `condominiousers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de tabela `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_condominio_id_foreign` FOREIGN KEY (`condominio_id`) REFERENCES `condominios` (`id`);

--
-- Limitadores para a tabela `condominios`
--
ALTER TABLE `condominios`
  ADD CONSTRAINT `condominios_imobiliaria_id_foreign` FOREIGN KEY (`imobiliaria_id`) REFERENCES `administradoras` (`id`),
  ADD CONSTRAINT `condominios_sindico_id_foreign` FOREIGN KEY (`sindico_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `horarios`
--
ALTER TABLE `horarios`
  ADD CONSTRAINT `horarios_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `horarios_condominio_id_foreign` FOREIGN KEY (`condominio_id`) REFERENCES `condominios` (`id`),
  ADD CONSTRAINT `horarios_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_administradora_id_foreign` FOREIGN KEY (`administradora_id`) REFERENCES `administradoras` (`id`),
  ADD CONSTRAINT `users_condominio_id_foreign` FOREIGN KEY (`condominio_id`) REFERENCES `condominios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
