<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImobiliarioToCondominios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('condominios', function (Blueprint $table) {
            $table->bigInteger('imobiliaria_id')->unsigned()->nullable();
            $table->foreign('imobiliaria_id')->references('id')->on('administradoras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('condominios', function (Blueprint $table) {
            //
        });
    }
}
