<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('condominio_id')->unsigned()->nullable();
            $table->foreign('condominio_id')->references('id')->on('condominios');
            $table->bigInteger('administradora_id')->unsigned()->nullable();
            $table->foreign('administradora_id')->references('id')->on('administradoras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('administradora_id');
            $table->dropColumn('condominio_id');
        });
    }
}
