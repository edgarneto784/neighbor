<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSindicoToCondominios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('condominios', function (Blueprint $table) {
            $table->BigInteger('sindico_id')->unsigned()->nullable();
            $table->foreign('sindico_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('condominios', function (Blueprint $table) {
            $table->dropColumn('sindico_id');
        });
    }
}
