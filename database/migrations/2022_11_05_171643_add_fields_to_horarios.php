<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToHorarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('horarios', function (Blueprint $table) {
            $table->BigInteger('condominio_id')->unsigned()->nullable();
            $table->BigInteger('usuario_id')->unsigned()->nullable();
            $table->BigInteger('area_id')->unsigned()->nullable();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('condominio_id')->references('id')->on('condominios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('horarios', function (Blueprint $table) {
            $table->dropForeign('horarios_condominio_id_foreign');
            $table->dropForeign('horarios_usuario_id_foreign');
            $table->dropForeign('horarios_area_id_foreign');
            $table->dropColumn('condominio_id');
            $table->dropColumn('usuario_id');
            $table->dropColumn('area_id');
        });
    }
}
