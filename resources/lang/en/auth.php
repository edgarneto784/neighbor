<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Credenciais não encontradas em nosso Sistema.',
    'password' => 'Combinação de Usuário e Senha Incorreta.',
    'throttle' => 'Muitas tentativas de Login. Tenta novamente em :seconds segundos.',

];
