<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sua Senha foi Reconfigurada!',
    'sent' => 'Acabamos de Enviar E-Mail de Reset de Senha!',
    'throttled' => 'Por Favor, Aguarde Antes de Tentar Novamente.',
    'token' => 'Este Token de Senha é Inválido.',
    'user' => "Não Encontramos um Usuário Registrado Nesta Conta de E-Mail.",

];
