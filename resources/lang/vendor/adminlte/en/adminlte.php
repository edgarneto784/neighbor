<?php

return [

    'full_name'                   => 'Nome Completo',
    'email'                       => 'E-Mail',
    'password'                    => 'Senha',
    'retype_password'             => 'Redigite a Senha',
    'remember_me'                 => 'Lembrar-me',
    'register'                    => 'Registrar',
    'register_a_new_membership'   => 'Registrar um Novo Usuário',
    'i_forgot_my_password'        => 'Esqueci Minha Senha',
    'i_already_have_a_membership' => 'Eu Já Sou Cadastrado',
    'sign_in'                     => 'Entrar',
    'log_out'                     => 'Sair',
    'toggle_navigation'           => 'Ativar Navegação',
    'login_message'               => 'Entre Para Iniciar Sua Sessão',
    'register_message'            => 'Registrar um Novo Usuário',
    'password_reset_message'      => 'Resetar Senha',
    'reset_password'              => 'Resetar Senha',
    'send_password_reset_link'    => 'Enviar Link de Reset de Senha',
    'verify_message'              => 'Sua Conta Precisa Ser Verificada',
    'verify_email_sent'           => 'Um Link de Verificação de Conta Foi Enviado á Seu E-mail.',
    'verify_check_your_email'     => 'Antes de Proceder, Por Gentileza Procure em Seu E-Mail Pelo Link de Verificação. Olhe Também no SPAM.',
    'verify_if_not_recieved'      => 'Se Você Não Recebeu o E-Mail',
    'verify_request_another'      => 'Clique Aqui Para Solicitar Outro',
    'confirm_password_message'    => 'Por Favor, Confirme Sua Senha Para Continuar',
    'remember_me_hint'            => 'Manter-me Autenticado Indefinidamente, Ou Até Eu Manualmente Sair.',
];
