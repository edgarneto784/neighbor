@extends('adminlte::page')
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.js"></script>
</head>
    @section('content')
    <h1 style="padding: 15px; text-align: center">Nova Administradora de Imóveis</h1>
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {!! Form::open(['route'=>'administradoras.store']) !!}
        <div class="form-group">
            {!! Form::label('Nome','Nome') !!}
            {!! Form::text('nome',null, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('telefone','Telefone') !!}
            {!! Form::text('telefone',null, ['Class'=>'phone form-control','required', 'placeholder'=>'(00) 00000-0000']) !!}

        </div>
        <div class="form-group">
            {!! Form::submit('Criar Administradora', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
    <script>  
        $(document).ready(function($){  
            $('.phone').inputmask('(99) 99999-9999');  
        });  
    </script>  
@stop
