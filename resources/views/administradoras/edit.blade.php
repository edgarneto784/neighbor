@extends('adminlte::page')
    @section('content')
    <h1>Editando Administradora: {{ $administradora->nome }}</h1>
    {!! Form::open(['route'=>["administradoras.update",'id'=>$administradora->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('Nome','Nome') !!}
            {!! Form::text('nome',$administradora->nome, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('telefone','telefone') !!}
            {!! Form::text('telefone',$administradora->telefone, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::submit('Editar Administradora', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop
