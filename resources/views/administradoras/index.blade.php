@extends('layouts.default')
    @section('content')
    <h1 style="padding: 15px; text-align: center">.:. Administradoras de Imóveis .:.</h1>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($administradoras as $administradora)
                <tr>
                    <td>{{ $administradora->nome }}</td>
                    <td>{{ $administradora->telefone }}</td>
                    <td>
                        <a href="{{ route('administradoras.edit', $administradora->id) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onClick="return ConfirmaExclusao({{$administradora->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $administradoras->links("pagination::bootstrap-4") }}
    <div class="row" style="justify-content: center; padding-top: 30px">
        <a href="{{ route('administradoras.create') }}" class="btn btn-primary">Nova Administradora</a>
        <a href="{{ route('relatorios.administradoras') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Administradoras</a>
    </div>
@stop

@section('table-delete')
"administradoras"
@endsection
