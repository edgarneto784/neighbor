@extends('adminlte::page')
    @section('content')
    <h1 style="padding: 15px; text-align: center">Cadastro de Nova Área</h1>
    {!! Form::open(['route'=>'areas.store']) !!}
        <div class="form-group">
            {!! Form::label('Nome','Nome') !!}
            {!! Form::text('nome',null, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('qtd_tempo','Quantidade de Horas de Cada Agendamento') !!}
            {!! Form::text('qtd_tempo',null, ['Class'=>'form-control','required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('valor','Valor em R$ por Agendamento') !!}
            {!! Form::number('valor',null, ['Class'=>'form-control','required','placeholder'=>'Apenas números']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('qtd_pessoas','Quantidade Máxima de Ocupantes') !!}
            {!! Form::number('qtd_pessoas',null, ['Class'=>'form-control','required']) !!}
        </div>

        <div>
            <input type="hidden" name="condominio_id" value="{{Auth::user()->condominio_id}}">
        </div>
        <div class="form-group">
            {!! Form::submit('Criar Area', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop
