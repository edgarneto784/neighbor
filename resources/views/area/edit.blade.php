@extends('adminlte::page')
    @section('content')
    <h1>Editando Areas: {{ $area->nome }}</h1>
    {!! Form::open(['route'=>["areas.update",'id'=>$area->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('Nome','Nome') !!}
            {!! Form::text('nome',$area->nome, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('qtd_tempo','Quantidade de tempo') !!}
            {!! Form::text('qtd_tempo',$area->qtd_tempo, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('valor','valor R$') !!}
            {!! Form::number('valor',$area->valor, ['Class'=>'form-control','required','placeholder'=>'Apenas números']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('qtd_pessoas','Quantidade de pessoas') !!}
            {!! Form::number('qtd_pessoas',$area->qtd_pessoas, ['Class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Editar Area', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop
