@extends('layouts.default')

<script>
    function sucesso(){
        alert('Parabéns! Horário Reservado com Sucesso!');
    }
    function error(){
        alert('Já Existe uma Reserva Nesse Horário!');
    }
</script>
@if (session('success'))
    <script>sucesso()</script>
@elseif (session('error'))
    <script>error()</script>
@endif


    @section('content')
    <h1 style="padding: 15px; text-align: center">.:. Áreas Comuns .:.</h1>
    @can('sudo')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Quantidade de Tempo</th>
            <th>Valor</th>
            <th>Quantidade de Pessoas</th>
            <th>Condominio</th>
        </thead>
        <tbody>
            @foreach($areas as $area)
                <tr>
                    <td>{{$area->nome }}</td>
                    <td>{{$area->qtd_tempo}}h</td>
                    <td>R$ {{$area->valor }}</td>
                    <td>{{$area->qtd_pessoas }}</td>
                    <td>{{$area->condominio->nome }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('relatorios.areas') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Áreas</a>
        </div>
    </div>
    @endcan
    @can('sindico')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Quantidade de Tempo</th>
            <th>Valor</th>
            <th>Quantidade de Pessoas</th>
            <th>Condominio</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($areas as $area)
                @if ($area->condominio_id == Auth::user()->condominio_id)
                <tr>
                    <td>{{$area->nome }}</td>
                    <td>{{$area->qtd_tempo}}h</td>
                    <td>R$ {{$area->valor }}</td>
                    <td>{{$area->qtd_pessoas }}</td>
                    <td>{{$area->condominio->nome }}</td>
                    @can('sindico')
                        <td>
                            <a href="{{ route('areas.edit', $area->id) }}" class="btn-sm btn-success">Editar</a>
                            <a href="#" onClick="return ConfirmaExclusao({{$area->id}})" class="btn-sm btn-danger">Remover</a>
                            {!! Form::open(['route'=>'horarios']) !!}
                            <input type="hidden" name="area_id" value="{{$area->id}}">
                            {!! Form::submit('Ver Horários', ['class'=>'btn-sm btn-primary', 'style'=>'margin-top: 5px']) !!}
                            {!! Form::close() !!}
                        </td>
                    @endcan
                    @can('morador')
                    {!! Form::open(['route'=>'horarios']) !!}
                        <td>
                            <input type="hidden" name="area_id" value="{{$area->id}}">
                            <div class="form-group">
                                {!! Form::submit('Ver Horários', ['class'=>'btn-sm btn-success']) !!}
                            </div>
                        </td>
                    {!! Form::close() !!}
                    @endcan 
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    @endcan
    @can('morador')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Quantidade de Tempo</th>
            <th>Valor</th>
            <th>Quantidade de Pessoas</th>
            <th>Condominio</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($areas as $area)
                @if ($area->condominio_id == Auth::user()->condominio_id)
                <tr>
                    <td>{{$area->nome }}</td>
                    <td>{{$area->qtd_tempo}}h</td>
                    <td>R$ {{$area->valor }}</td>
                    <td>{{$area->qtd_pessoas }}</td>
                    <td>{{$area->condominio->nome }}</td>
                    {!! Form::open(['route'=>'horarios']) !!}
                        <td>
                            <input type="hidden" name="area_id" value="{{$area->id}}">
                            <div class="form-group">
                                {!! Form::submit('Ver Horários', ['class'=>'btn-sm btn-success']) !!}
                            </div>
                        </td>
                    {!! Form::close() !!}
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    @endcan
    {{ $areas->links("pagination::bootstrap-4") }}
    @can('sindico')
        <div class="row" style="justify-content: center; padding-top: 30px" >
            <a href="{{ route('areas.create') }}" class="btn btn-primary">Nova Área</a>
            <a href="{{ route('relatorios.areas') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Áreas</a>
        </div>
    @endcan
@stop

@section('table-delete')
"areas"
@endsection
