@extends('adminlte::page')
    @section('content')
    @can('sudo')
        {!! Form::open(['url'=>'/usuarios/store']) !!}
        <div class="form-group">
            {!! Form::label('name','Nome') !!}
            {!! Form::text('name',null, ['Class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email','E-Mail') !!}
            {!! Form::text('email',null, ['Class'=>'form-control','required']) !!}
        </div>

        {!! Form::label('administradora_id','Administradora Pertencente') !!}
        {!! Form::select('administradora_id',\App\Models\administradora::orderBy('nome')->pluck('nome','id')->toArray(),null, ['Class'=>'form-control','required']) !!}

        <div class="form-group">
            {!! Form::label('password','Senha') !!}
            {!! Form::password('password', array('id' => 'password', "class" => "form-control", 'required')) !!}
        </div>

        <input type="hidden" name="permissao" value="2">
        <div class="form-group">
            {!! Form::submit('Criar Usuário', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar Campos', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
        
    @endcan
    @can('admin')
        <h1 style="padding: 15px; text-align: center">.:. Insira Novo Corretor ou Síndico .:.</h1>
        <div class="form-group">

        </div>
            {!! Form::open(['url'=>'/usuarios/store']) !!}
            <div class="form-group">
                {!! Form::label('name','Nome') !!}
                {!! Form::text('name',null, ['Class'=>'form-control','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email','E-Mail') !!}
                {!! Form::text('email',null, ['Class'=>'form-control','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('permissao','Perfil') !!}
                {!! Form::select('permissao',array(
                    '2'=>'Corretor',
                    '3'=>'Sindico',
                ),null, ['Class'=>'form-control','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password','Senha') !!}
                {!! Form::password('password', array('id' => 'password', "class" => "form-control", 'required')) !!}
            </div>
            <div>
                <input type="hidden" name="administradora_id" value="{{Auth::user()->administradora_id}}">
            </div>
            <div class="form-group">
                {!! Form::submit('Criar Usuário', ['class'=>'btn btn-primary']) !!}
                {!! Form::reset('Limpar Campos', ['class'=>'btn btn-default']) !!}
            </div>
            {!! Form::close() !!}
    @endcan
    @can('sindico')
        <h1 style="padding: 15px; text-align: center">.:. Insira Novo Condômino .:.</h1>
        <div class="form-group">

        </div>
            {!! Form::open(['url'=>'/usuarios/store']) !!}
            <div class="form-group">
                {!! Form::label('name','Nome') !!}
                {!! Form::text('name',null, ['Class'=>'form-control','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email','E-Mail') !!}
                {!! Form::text('email',null, ['Class'=>'form-control','required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password','Senha') !!}
                {!! Form::password('password', array('id' => 'password', "class" => "form-control", 'required')) !!}
            </div>
            <div>
                <input type="hidden" name="condominio_id" value="{{Auth::user()->condominio_id}}">
                <input type="hidden" name="administradora_id" value="{{Auth::user()->administradora_id}}">
                <input type="hidden" name="permissao" value="4">
            </div>
            <div class="form-group">
                {!! Form::submit('Criar Usuário', ['class'=>'btn btn-primary']) !!}
                {!! Form::reset('Limpar Campos', ['class'=>'btn btn-default']) !!}
            </div>
            {!! Form::close() !!}
    @endcan
    @can('morador')
        <script>
            alert('Você não tem permissão para acessar essa página!');
            window.location = "/home";
        </script>
    @endcan
@stop