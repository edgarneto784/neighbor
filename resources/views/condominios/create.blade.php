@extends('adminlte::page')
    @can('morador')
        <script>
            alert('Você não tem permissão para acessar essa página!');
            window.location = "/home";
        </script>
    @endcan
    @can('sindico')
        <script>
            alert('Você não tem permissão para acessar essa página!');
            window.location = "/home";
        </script>
    @endcan
    @section('content')
    <h1 style="padding: 15px; text-align: center">Insira Novo Condomínio</h1>
    {!! Form::open(['url'=>'/condominios/store']) !!}
        <div class="form-group">
            {!! Form::label('nome','Nome do Condomínio') !!}
            {!! Form::text('nome',null, ['Class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('endereco','Endereço') !!}
            {!! Form::text('endereco',null, ['Class'=>'form-control','required']) !!}
        </div>
        <div>
            <input type="hidden" name="imobiliaria_id" value="{{Auth::user()->administradora_id}}">
        </div>
        <div class="form-group">
            {!! Form::submit('Criar Condomínio', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop
