@extends('adminlte::page')
    @section('content')
    @can('morador')
        <script>
            alert('Você não tem permissão para acessar essa página!');
            window.location = "/home";
        </script>
    @endcan
    @can('sindico')
        <script>
            alert('Você não tem permissão para acessar essa página!');
            window.location = "/home";
        </script>
    @endcan
    <h1>Editando Condomínio</h1>
    {!! Form::open(['route'=>["condominios.update",'id'=>$condominio->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('Nome','Nome') !!}
            {!! Form::text('nome',$condominio->nome, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('endereco','Endereço') !!}
            {!! Form::text('endereco',$condominio->endereco, ['Class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('imobiliaria_id','Imobiliária Responsável') !!}
            {!! Form::select('imobiliaria_id',\App\Models\administradora::orderBy('nome')->pluck('nome','id')->toArray(),$condominio->imobiliaria_id, ['Class'=>'form-control','required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Editar Condomínio', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar Campos', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop
