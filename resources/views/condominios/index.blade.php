@extends('layouts.default')
    @section('content')
    <h1 style="padding: 15px; text-align: center">Condomínios</h1>
    @can('sudo')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Imobiliária</th>
        </thead>
        <tbody>
            @foreach($condominios as $condominio)
                <tr>
                    <td>{{ $condominio->nome }}</td>
                    <td>{{ $condominio->Endereco }}</td>
                    <td>{{ $condominio->imobiliaria->nome }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $condominios->links("pagination::bootstrap-4") }}
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('relatorios.condominios') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Condomínios</a>
        </div>
    </div>
    @endcan
    @can('admin')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Imobiliária</th>
            <th>Sindico</th>
            <th>Telefone</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($condominios as $condominio)
                @if ($condominio->imobiliaria_id == Auth::user()->administradora_id)
                    <tr>
                        <td>{{ $condominio->nome }}</td>
                        <td>{{ $condominio->Endereco }}</td>
                        <td>{{ $condominio->imobiliaria->nome }}</td>
                        @if ($condominio->sindico_id == null)
                            <td>Não possui</td>
                        @else
                            <td>{{ $condominio->sindico->name }}</td>
                        @endif
                        @if ($condominio->telefone == null)
                            <td>Não possui</td>
                        @else
                            <td>{{ $condominio->telefone }}</td>
                        @endif
                        <td>
                            <a href="{{ route('condominios.edit', $condominio->id) }}" class="btn-sm btn-success">Editar</a>
                            <a href="{{ route('condominios.sindico', $condominio->id) }}" class="btn-sm btn-warning">Sindico</a>
                            <a href="#" onClick="return ConfirmaExclusao({{$condominio->id}})" class="btn-sm btn-danger">Remover</a>
                        </td>
                    </tr>
                    
                @endif
            @endforeach
        </tbody>
    </table>
    {{ $condominios->links("pagination::bootstrap-4") }}
    <div class="row" style="justify-content: center; padding-top: 30px" >
        <a href="{{ route('condominios.create') }}" class="btn btn-primary">Novo Condominio</a>
        <a href="{{ route('relatorios.condominios') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Condomínios</a>
    </div>
    @endcan
@stop

@section('table-delete')
"condominios"
@endsection
