@extends('adminlte::page')
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.js"></script>
</head>

    @section('content')
    <h1 style="padding: 15px; text-align: center">.:. Aponte o Síndico do Condomínio .:.</h1>
    {!! Form::open(['route'=>["condominios.update_sindico",'id'=>$condominio->id], 'method'=>'put']) !!}    
        <div class="form-group">
            {!! Form::label('sindico_id','Nome do Síndico') !!}
            {!! Form::select('sindico_id',\App\Models\User::where('administradora_id', [Auth::user()->administradora_id] )->whereRaw('id in (select sindico_id from condominios where id = ?) or condominio_id is null and permissao = 3',[$condominio->id])->pluck('name','id'),$condominio->sindico_id,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('telefone','Telefone do Síndico (Público para os Condôminos)') !!}
            {!! Form::text('telefone',$condominio->telefone, ['Class'=>'phone form-control','required', 'placeholder'=>'(00) 00000-0000']) !!}
        </div>
        <input type="hidden" name="condominio_id" value="{{$condominio->id}}">
        <div class="form-group">
            {!! Form::submit('Salvar Síndico', ['class'=>'btn btn-primary']) !!}
            <a href="{{ URL::previous() }}" class="btn btn-default">Voltar</a>
        </div>
    {!! Form::close() !!}
    <script>  
        $(document).ready(function($){  
            $('.phone').inputmask('(99) 99999-9999');  
        });  
    </script>  
@stop