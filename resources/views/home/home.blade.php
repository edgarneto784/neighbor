@extends('layouts.default')
    @section('content')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
    
    <h1 style="padding: 15px; text-align: center">.:. Bem Vindo, {{ Auth::user()->name }} .:.</h1>
    @can('sudo')
    <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                <h2>{{ $administradora->count() }}</h2>
  
                  <p>Total de Administradoras</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-calculator"></i>
                </div>
                <a href="{{ route('administradoras') }}" class="small-box-footer">Ver Administradoras<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h2>{{ $condominio->count() }}</h2>
  
                  <p>Total de Condomínios</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('condominios') }}" class="small-box-footer">Ver Condomínios<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h2>{{ $user->count() }}</h2>
  
                  <p>Usuários Registrados</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route('usuarios') }}" class="small-box-footer">Total de Usuários<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h2>{{ $areas->count() }}</h2>
  
                  <p>Total de Áreas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-calendar"></i>
                </div>
                <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
        </div>
            <!-- /.row -->
            
    </section>
    @endcan

    
    @can('admin')
    <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                <h2>{{ $condominio->where('imobiliaria_id', Auth::user()->administradora_id)->count() }}</h2>
  
                  <p>Total de Condomínios</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-home"></i>
                </div>
                <a href="{{ route('condominios') }}" class="small-box-footer">Ver Condomínios<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                    <h2>{{ $user->where('administradora_id', Auth::user()->administradora_id)->where('permissao','2')->count() }}</h2>
  
                  <p>Total de Corretores</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route('usuarios') }}" class="small-box-footer">Listar Usuários<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h2>{{ $user->where('administradora_id', Auth::user()->administradora_id)->where('permissao','3')->count() }}</h2>
  
                  <p>Total de Síndicos</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route('usuarios') }}" class="small-box-footer">Listar Usuários<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h2>{{ $user->where('administradora_id', Auth::user()->administradora_id)->where('permissao','4')->count() }}</h2>
  
                  <p>Total de Moradores</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route('usuarios') }}" class="small-box-footer">Listar Usuários<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
        </div>
            <!-- /.row -->
            
    </section>
    @endcan


    @can('sindico')
    <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-12">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                <h2>{{ $areas->where('condominio_id', Auth::user()->condominio_id)->count() }}</h2>
  
                  <p>Total de Áreas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-calendar"></i>
                </div>
                <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-12 col-12">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h2>{{ $user->where('administradora_id', Auth::user()->administradora_id)->where('permissao','4')->count() }}</h2>
  
                  <p>Total de Moradores</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route('usuarios') }}" class="small-box-footer">Ver Moradores<i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-12 col-12">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h2>{{ $horario->where('condominio_id',Auth::user()->condominio_id)->where('tempo_inicio', '<=', Carbon\Carbon::now()->addDays(30))->where('tempo_inicio', '>=', Carbon\Carbon::now())->count() }}</h2>
  
                  <p>Total de Agendamentos do condomínio nos Próximos<strong> 30 </strong>Dias.</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-calendar"></i>
                </div>
                <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-12 col-12">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                <h2>{{ $horario->where('usuario_id', Auth::user()->id)->where('tempo_inicio', '>=', Carbon\Carbon::now()->subDays(30))->where('tempo_inicio', '<=', Carbon\Carbon::now())->count() }}</h2>
  
                  <p>Total de meus Agendamentos nos Últimos<strong> 30 </strong>Dias.</p>
                </div>
                <div class="icon">
                  <i class="ion ion-calendar"></i>
                </div>
                <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-12 col-12">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                <h2>{{ $horario->where('usuario_id', Auth::user()->id)->where('tempo_inicio', '<=', Carbon\Carbon::now()->addDays(30))->where('tempo_inicio', '>=', Carbon\Carbon::now())->count() }}</h2>
  
                  <p>Total de meus Agendamentos nos Próximos<strong> 30 </strong>Dias.</p>
                </div>
                <div class="icon">
                  <i class="ion ion-calendar"></i>
                </div>
                <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
        </div>
            <!-- /.row -->
            
    </section>
    @endcan
    @can('morador')
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      <div class="row">
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h2>{{ $areas->where('condominio_id', Auth::user()->condominio_id)->count() }}</h2>

                <p>Total de Áreas</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-calendar"></i>
              </div>
              <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h2>{{ $horario->where('usuario_id', Auth::user()->id)->count() }}</h2>

                <p>Total de Agendamentos</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-calendar"></i>
              </div>
              <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              <h2>{{ $horario->where('usuario_id', Auth::user()->id)->where('tempo_inicio', '>=', Carbon\Carbon::now()->subDays(30))->where('tempo_inicio', '<=', Carbon\Carbon::now())->count() }}</h2>

                <p>Total de Agendamentos nos Últimos<strong> 30 </strong>Dias.</p>
              </div>
              <div class="icon">
                <i class="ion ion-calendar"></i>
              </div>
              <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-12 col-12">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <h2>{{ $horario->where('usuario_id', Auth::user()->id)->where('tempo_inicio', '<=', Carbon\Carbon::now()->addDays(30))->where('tempo_inicio', '>=', Carbon\Carbon::now())->count() }}</h2>

                <p>Total de Agendamentos nos Próximos<strong> 30 </strong>Dias.</p>
              </div>
              <div class="icon">
                <i class="ion ion-calendar"></i>
              </div>
              <a href="{{ route('areas') }}" class="small-box-footer">Ver Áreas <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
      </div>     
  </section>
    @endcan
@stop