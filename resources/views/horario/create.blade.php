@extends('adminlte::page')
    @section('content')
    <h1>Nova Agendamento: {{$area->nome}}</h1>
    <h3 style="padding-top: 20px;padding-left: 20px">Tempo de Locação: {{$area->qtd_tempo}}h</h3>
    <h3 style="padding-left: 20px">Valor: R${{$area->valor}}</h3>
    <h3 style="padding-left: 20px">Quantidade Máxima de Pessoas: {{$area->qtd_pessoas}}</h3>
    <h3 style="padding-left: 20px">Horário de Inicio: {{carbon\carbon::parse($tempo_inicio)->format('d/m/Y - H:i')}}h</h3>
    <h3 style="padding-left: 20px;padding-bottom: 20px">Horário de Fim: {{carbon\carbon::parse($tempo_final)->format('d/m/Y - H:i')}}h</h3>

    {!! Form::open(['url'=>'/horarios/store']) !!}
        <div>
            <input type="hidden" name="tempo_inicio" value="{{$tempo_inicio}}">
            <input type="hidden" name="qtd_tempo" value="{{$area->qtd_tempo}}">
            <input type="hidden" name="area_id" value="{{$area->id}}">
        <div class="form-group">
            {!! Form::submit('Locar Horário', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop
