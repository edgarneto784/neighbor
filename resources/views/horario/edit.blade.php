@extends('adminlte::page')
    @section('content')
    <h1>Nova Agendamento: {{$horario->area->nome}}</h1>
    <h3 style="padding-top: 20px;padding-left: 20px">Tempo de locacao: {{$horario->area->qtd_tempo}}h</h3>
    <h3 style="padding-left: 20px">Valor: R${{$horario->area->valor}}</h3>
    <h3 style="padding-left: 20px;padding-bottom: 20px">Quantidade Máxima de Pessoas: {{$horario->area->qtd_pessoas}}</h3>
    {!! Form::open(['route'=>["horarios.update",'id'=>$horario->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('tempo_inicio','Horario Inicial') !!}
            {{ Form::input('dateTime-local', 'tempo_inicio', $horario->tempo_inicio, ['class' => 'form-control', 'required']) }}
        </div>
        <div>
            <input type="hidden" name="qtd_tempo" value="{{$horario->area->qtd_tempo}}">
            <input type="hidden" name="area_id" value="{{$horario->area->id}}">
        <div class="form-group">
            {!! Form::submit('Locar Horário', ['class'=>'btn btn-primary']) !!}
            <a href="{{route('areas')}}" class="btn btn-default">Voltar</a>
        </div>
    {!! Form::close() !!}
@stop
