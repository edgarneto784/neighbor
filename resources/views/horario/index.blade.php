@extends('layouts.default')
@section('content')
    <h1 style="padding-top: 15px; text-align: center">Agendamentos</h1>
    <link href='fullcalendar/main.css' rel='stylesheet' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
    <script src='fullcalendar/main.js'></script>
    <script src='fullcalendar/locales/pt-br.js'></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {

            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                locale: 'pt-br',
                initialView: 'timeGridWeek',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    // right: 'novo dayGridMonth,timeGridWeek,timeGridDay'
                     right: 'dayGridMonth,timeGridWeek,timeGridDay'

                },
                // customButtons: {
                //     novo: {
                //         text: 'Novo Agendamento',
                //         click: function() {
                //             window.location.href = ("agendas/create");
                //         }
                //     }
                // },
                dateClick: function(info) {
                    var data = info.dateStr;
                    window.location.href = ("horarios/create/{{$area}}/" + data);
                },
                events: [
                    @foreach($horario as $horarios)
                        {
                            id: {{ $horarios->id }},
                            title: '{{$areas->nome}} - {{$horarios->user->name}}',
                            start: '{{ $horarios->inicio }}',
                            end: '{{ $horarios->final }}',
                            extendedProps: {
                                user_id: '{{$horarios->usuario_id}}',
                            },
                        },
                    @endforeach
                ],
                eventClick: function(info) {

                    if( info.event.extendedProps.user_id == {{ Auth::user()->id }} ){
                    info.jsEvent.preventDefault(); // don't let the browser navigate          
                    $('#vizualizarComp #title').text(info.event.title)
                    $('#vizualizarComp #start').text(info.event.start.toLocaleString())
                    $('#vizualizarComp #end').text(info.event.end.toLocaleString())
                    $('#vizualizarComp').modal('show')
                    $("#edit").attr("href", 'horarios/' + info.event.id + '/edit')
                    $("#delete").attr("href", 'horarios/' + info.event.id + '/destroy')
                    }else{
                    info.jsEvent.preventDefault(); // don't let the browser navigate          
                    $('#vizualizar #id').text(info.event.id)
                    $('#vizualizar #title').text(info.event.title)
                    $('#vizualizar #start').text(info.event.start.toLocaleString())
                    $('#vizualizar #end').text(info.event.end.toLocaleString())
                    $('#vizualizar').modal('show')
                    }
                },
            });
            calendar.render();
        });
    </script>

    <!-- Calendar -->
    <div id='calendar'></div>

    <!-- Modal -->
    <div class="modal fade" id="vizualizarComp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalhes do Evento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="vizualizarComp">
                        <dl class="row">
                            <dt class="col-sm-3">Status Agendamento</dt>
                            <dd class="col-sm-9" id="title"></dd>
                            <dt class="col-sm-3">Inicio Agendamento</dt>
                            <dd class="col-sm-9" id="start"></dd>
                            <dt class="col-sm-3">Fim Agendamento</dt>
                            <dd class="col-sm-9" id="end"></dd>    
                        </dl>
                        <div class="form-group" style="text-align:center">
                            <a id="edit" href="" class="btn btn-outline-success">Editar</a>
                            <a id="delete" href="" class="btn btn-outline-danger">Excluir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="modal fade" id="vizualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalhes do Evento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="vizualizar">
                        <dl class="row">
                            <dt class="col-sm-3">Status Agendamento</dt>
                            <dd class="col-sm-9" id="title"></dd>
                            <dt class="col-sm-3">Inicio Agendamento</dt>
                            <dd class="col-sm-9" id="start"></dd>
                            <dt class="col-sm-3">Fim Agendamento</dt>
                            <dd class="col-sm-9" id="end"></dd>    
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
