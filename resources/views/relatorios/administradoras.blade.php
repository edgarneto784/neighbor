
    <h1 style="padding: 15px; text-align: center">.:. Administradoras de Imóveis .:.</h1>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Telefone</th>
        </thead>
        <tbody>
            @foreach($administradoras as $administradora)
                <tr>
                    <td>{{ $administradora->nome }}</td>
                    <td>{{ $administradora->telefone }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@section('table-delete')
"administradoras"
@endsection
