    <h1 style="padding: 15px; text-align: center">.:. Áreas Comuns .:.</h1>
    @can('sudo')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Quantidade de Tempo</th>
            <th>Valor</th>
            <th>Quantidade de Pessoas</th>
            <th>Condominio</th>
        </thead>
        <tbody>
            @foreach($areas as $area)
                <tr>
                    <td>{{$area->nome }}</td>
                    <td>{{$area->qtd_tempo}}h</td>
                    <td>R$ {{$area->valor }}</td>
                    <td>{{$area->qtd_pessoas }}</td>
                    <td>{{$area->condominio->nome }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @endcan
    @can('sindico')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Quantidade de Tempo</th>
            <th>Valor</th>
            <th>Quantidade de Pessoas</th>
            <th>Condominio</th>
        </thead>
        <tbody>
            @foreach($areas as $area)
                @if ($area->condominio_id == Auth::user()->condominio_id)
                <tr>
                    <td>{{$area->nome }}</td>
                    <td>{{$area->qtd_tempo}}h</td>
                    <td>R$ {{$area->valor }}</td>
                    <td>{{$area->qtd_pessoas }}</td>
                    <td>{{$area->condominio->nome }}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    @endcan
    @can('morador')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Quantidade de Tempo</th>
            <th>Valor</th>
            <th>Quantidade de Pessoas</th>
            <th>Condominio</th>
        </thead>
        <tbody>
            @foreach($areas as $area)
                @if ($area->condominio_id == Auth::user()->condominio_id)
                <tr>
                    <td>{{$area->nome }}</td>
                    <td>{{$area->qtd_tempo}}h</td>
                    <td>R$ {{$area->valor }}</td>
                    <td>{{$area->qtd_pessoas }}</td>
                    <td>{{$area->condominio->nome }}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    @endcan

@section('table-delete')
"areas"
@endsection