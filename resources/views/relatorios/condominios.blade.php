<h1 style="padding: 15px; text-align: center">Condomínios</h1>
    @can('sudo')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Imobiliária</th>
        </thead>
        <tbody>
            @foreach($condominios as $condominio)
                <tr>
                    <td>{{ $condominio->nome }}</td>
                    <td>{{ $condominio->Endereco }}</td>
                    <td>{{ $condominio->imobiliaria->nome }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @endcan
    @can('admin')
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Imobiliária</th>
            <th>Sindico</th>
            <th>Telefone</th>
        </thead>
        <tbody>
            @foreach($condominios as $condominio)
                @if ($condominio->imobiliaria_id == Auth::user()->administradora_id)
                    <tr>
                        <td>{{ $condominio->nome }}</td>
                        <td>{{ $condominio->Endereco }}</td>
                        <td>{{ $condominio->imobiliaria->nome }}</td>
                        @if ($condominio->sindico_id == null)
                            <td>Não possui</td>
                        @else
                            <td>{{ $condominio->sindico->name }}</td>
                        @endif
                        @if ($condominio->telefone == null)
                            <td>Não possui</td>
                        @else
                            <td>{{ $condominio->telefone }}</td>
                        @endif
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    @endcan
@section('table-delete')
"condominios"
@endsection
