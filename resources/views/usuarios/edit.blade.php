@extends('adminlte::page')
    @section('content')
    <h1>Editando Usuario: {{ $usuario->name }}</h1>
    {!! Form::open(['route'=>["usuarios.update",'id'=>$usuario->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('Nome','Nome') !!}
            {!! Form::text('name',$usuario->name, ['Class'=>'form-control','required']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('email','Email') !!}
            {!! Form::text('email',$usuario->email, ['Class'=>'form-control','required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Editar Usuario', ['class'=>'btn btn-primary']) !!}
            <a href="{{ URL::previous() }}" class="btn btn-default">Voltar</a>
        </div>
    {!! Form::close() !!}
@stop
