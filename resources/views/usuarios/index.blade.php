@extends('layouts.default')
    @section('content')
    <h1 style="padding: 15px; text-align: center">.:. Usuários Neighbor .:.</h1>
    <table class="table table-stripe table-bordered table-hover">
    @can('sudo')
        <thead>
            <th>Nome</th>
            <th>E-Mail</th>
            <th>Administradora</th>
            <th>Condomínio</th>
            <th>Permissão</th>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
                <tr>
                    <td>{{ $usuario->name }}</td>
                    <td>{{ $usuario->email }}</td>
                    @if ($usuario->administradora_id != null)
                        <td>{{ $usuario->imobiliaria->nome }}</td>
                    @else
                        <td>Usuario Admin</td>
                    @endif
                    @if ($usuario->condominio_id != null)
                        <td>{{ $usuario->condominio->nome }}</td>
                    @else
                        <td>Usuário sem Condomínio</td>
                    @endif
                    @if ($usuario->permissao == 1)
                        <td>Administrador</td>
                    @elseif ($usuario->permissao == 2)
                        <td>Corretor</td>
                    @elseif ($usuario->permissao == 3)
                        <td>Sindico</td>
                    @elseif ($usuario->permissao == 4)
                        <td>Condomino</td>
                    @endif
                    <td>
                        <a href="#" onClick="return ConfirmaExclusao({{$usuario->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $usuarios->links("pagination::bootstrap-4") }}
    <div class="row" style="justify-content: center; padding-top: 10px; padding-bottom:10px">
        <a href="{{ route('usuarios.create') }}" class="btn btn-primary">Novo Usuário</a>
        <a href="{{ route('relatorios.users') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Usuários</a>
    </div>
    @endcan
    @can('admin')
        <thead>
            <th>Nome</th>
            <th>E-Mail</th>
            <th>Nível de Acesso</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
                @if ($usuario->administradora_id == Auth::user()->administradora_id)
                    <tr>
                        <td>{{ $usuario->name }}</td>
                        <td>{{ $usuario->email }}</td>
                        @if ($usuario->permissao == 2)
                            <td>Corretor</td>
                        @elseif ($usuario->permissao == 3)
                            <td>Sindico</td>
                        @else
                            <td>Morador</td>
                        @endif
                        <td>
                            <a href="#" onClick="return ConfirmaExclusao({{$usuario->id}})" class="btn-sm btn-danger">Remover</a>
                        </td>
                    </tr>
                    
                @endif
            @endforeach
        </tbody>
    </table>
    {{ $usuarios->links("pagination::bootstrap-4") }}
    <div class="row" style="justify-content: center; padding-top: 30px">
        <a href="{{ route('usuarios.create') }}" class="btn btn-primary">Novo Usuário</a>
        <a href="{{ route('relatorios.users') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Usuários</a> 
    </div>
    @endcan
    @can('sindico')
        <thead>
            <th>Nome</th>
            <th>E-Mail</th>
            <th>Nível de Acesso</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
                @if ($usuario->administradora_id == Auth::user()->administradora_id && $usuario->permissao != 2 && $usuario->permissao != 1 && $usuario->condominio_id == Auth::user()->condominio_id)
                    <tr>
                        <td>{{ $usuario->name }}</td>
                        <td>{{ $usuario->email }}</td>
                        @if ($usuario->permissao == 3)
                            <td>Sindico</td>
                        @else
                            <td>Morador</td>
                        @endif
                        <td>
                            <a href="#" onClick="return ConfirmaExclusao({{$usuario->id}})" class="btn-sm btn-danger">Remover</a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    {{ $usuarios->links("pagination::bootstrap-4") }}
    <div class="row" style="justify-content: center; padding-top: 30px">
        <a href="{{ route('usuarios.create') }}" class="btn btn-primary">Novo Usuário</a>
        <a href="{{ route('relatorios.users') }}" class="btn btn-primary" style="margin-left: 10px">Relatório de Usuários</a>
    </div>
    @endcan
    @can('morador')
        <script>
            alert('Você não tem permissão para acessar essa página!');
            window.location = "/home";
        </script>
    @endcan
    
@stop
@section('table-delete')
"usuarios"
@endsection
