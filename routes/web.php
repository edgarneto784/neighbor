<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'App\Http\Controllers\HomeController@index');
    // Administradoras
    Route::group(['prefix'=>'administradoras','where'=>['id'=>'[0-9]+']], function(){
        Route::get('', ['as'=>'administradoras', 'uses'=>'App\Http\Controllers\AdministradoraController@index']);
        Route::get('create', ['as'=>'administradoras.create', 'uses'=>'App\Http\Controllers\AdministradoraController@create']);
        Route::post('store', ['as'=>'administradoras.store', 'uses'=>'App\Http\Controllers\AdministradoraController@store']);
        Route::get('{id}/destroy', ['as'=>'administradoras.destroy', 'uses'=>'App\Http\Controllers\AdministradoraController@destroy']);
        Route::get('{id}/edit', ['as'=>'administradoras.edit', 'uses'=>'App\Http\Controllers\AdministradoraController@edit']);
        Route::put('{id}/update', ['as'=>'administradoras.update', 'uses'=>'App\Http\Controllers\AdministradoraController@update']);
    });

    // Condominios
    Route::group(['prefix'=>'condominios','where'=>['id'=>'[0-9]+']], function(){
        Route::get('', ['as'=>'condominios', 'uses'=>'App\Http\Controllers\CondominioController@index']);
        Route::get('create', ['as'=>'condominios.create', 'uses'=>'App\Http\Controllers\CondominioController@create']);
        Route::post('store', ['as'=>'condominios.store', 'uses'=>'App\Http\Controllers\CondominioController@store']);
        Route::get('{id}/destroy', ['as'=>'condominios.destroy', 'uses'=>'App\Http\Controllers\CondominioController@destroy']);
        Route::get('{id}/edit', ['as'=>'condominios.edit', 'uses'=>'App\Http\Controllers\CondominioController@edit']);
        Route::put('{id}/update', ['as'=>'condominios.update', 'uses'=>'App\Http\Controllers\CondominioController@update']);
        Route::get('{id}/sindico', ['as'=>'condominios.sindico', 'uses'=>'App\Http\Controllers\CondominioController@sindico']);
        Route::put('{id}/update_sindico', ['as'=>'condominios.update_sindico', 'uses'=>'App\Http\Controllers\CondominioController@update_sindico']);
    });

    // Areas
    Route::group(['prefix'=>'areas','where'=>['id'=>'[0-9]+']], function(){
        Route::get('', ['as'=>'areas', 'uses'=>'App\Http\Controllers\AreasController@index']);
        Route::get('create', ['as'=>'areas.create', 'uses'=>'App\Http\Controllers\AreasController@create']);
        Route::post('store', ['as'=>'areas.store', 'uses'=>'App\Http\Controllers\AreasController@store']);
        Route::get('{id}/destroy', ['as'=>'areas.destroy', 'uses'=>'App\Http\Controllers\AreasController@destroy']);
        Route::get('{id}/edit', ['as'=>'areas.edit', 'uses'=>'App\Http\Controllers\AreasController@edit']);
        Route::put('{id}/update', ['as'=>'areas.update', 'uses'=>'App\Http\Controllers\AreasController@update']);
    });

    //usuarios
    Route::group(['prefix'=>'usuarios','where'=>['id'=>'[0-9]+']], function(){
        Route::get('', ['as'=>'usuarios', 'uses'=>'App\Http\Controllers\UsuariosController@index']);
        Route::get('create', ['as'=>'usuarios.create', 'uses'=>'App\Http\Controllers\UsuariosController@create']);
        Route::post('store', ['as'=>'usuarios.store', 'uses'=>'App\Http\Controllers\UsuariosController@store']);
        Route::get('{id}/destroy', ['as'=>'usuarios.destroy', 'uses'=>'App\Http\Controllers\UsuariosController@destroy']);
        Route::get('{id}/edit', ['as'=>'usuarios.edit', 'uses'=>'App\Http\Controllers\UsuariosController@edit']);
        Route::put('{id}/update', ['as'=>'usuarios.update', 'uses'=>'App\Http\Controllers\UsuariosController@update']);
    });

    //horarios
    Route::group(['prefix'=>'horarios','where'=>['id'=>'[0-9]+']], function(){
        Route::post('', ['as'=>'horarios', 'uses'=>'App\Http\Controllers\HorariosController@index']);
        //Route::get('{id}', ['as'=>'horarios', 'uses'=>'App\Http\Controllers\HorariosController@index']);
        Route::get('create/{id}/{tempo_inicio}', ['as'=>'horarios.create', 'uses'=>'App\Http\Controllers\HorariosController@create']);
        Route::post('store', ['as'=>'horarios.store', 'uses'=>'App\Http\Controllers\HorariosController@store']);
        Route::get('{id}/destroy', ['as'=>'horarios.destroy', 'uses'=>'App\Http\Controllers\HorariosController@destroy']);
        Route::get('{id}/edit', ['as'=>'horarios.edit', 'uses'=>'App\Http\Controllers\HorariosController@edit']);
        Route::put('{id}/update', ['as'=>'horarios.update', 'uses'=>'App\Http\Controllers\HorariosController@update']);
    });

    //relatorios
    Route::group(['prefix'=>'relatorios','where'=>['id'=>'[0-9]+']], function(){
        Route::get('users', ['as'=>'relatorios.users', 'uses'=>'App\Http\Controllers\RelatorioController@users']);
        Route::get('areas', ['as'=>'relatorios.areas', 'uses'=>'App\Http\Controllers\RelatorioController@areas']);
        Route::get('condominios', ['as'=>'relatorios.condominios', 'uses'=>'App\Http\Controllers\RelatorioController@condominios']);
        Route::get('administradoras', ['as'=>'relatorios.administradoras', 'uses'=>'App\Http\Controllers\RelatorioController@administradoras']);
    });

});
